/*
 * Copyright 2020 Siqura B.V.
 *
 * Author: Ari Sadarjoen <a.sadarjoen@siqura.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <asm/gpio.h>
#include <asm/mach-imx/iomux-v3.h>
#include <dm/device.h>
#include <os.h>
#include <stdlib.h>

#define ACTIVE_STATE 0

bool button_pressed(const int gpio_nr)
{
    return ACTIVE_STATE == gpio_get_value(gpio_nr);
}

bool reset_pressed(const int gpio_nr, const unsigned int duration_sec)
{
    int count = 0;

    if (0 == duration_sec) {
        return false;
    }
    count = 10 * duration_sec;
    printf (" pressed for > %d sec?: ", duration_sec);

    while (count > 0) {
        if (! button_pressed(gpio_nr)) {
            break;
        }
        udelay(100000);
        count--;
    }

    if (count > 0) {
        printf ("no\n");
    }
    else {
        printf ("YES\n");
    }

    return (0 == count);
}

void detect_reset(int gpio_nr, int params_wait_s, int fw_wait_s)
{
    if (gpio_nr < 0) {
        printf ("Warning: wrong gpio_nr %d\n", gpio_nr);
        return;
    }

    gpio_request(gpio_nr, "gpio");
    gpio_direction_input(gpio_nr);

    printf ("Restore settings -");
    if (!reset_pressed(gpio_nr, params_wait_s)) {
        return;
    }
    env_set("restoredefaults", "true");

    printf ("Restore firmware -");
    if (!reset_pressed(gpio_nr, fw_wait_s)) {
        return;
    }
    env_set("restorefirmware", "true");
}
